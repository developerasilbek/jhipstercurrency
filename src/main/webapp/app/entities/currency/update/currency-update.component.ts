import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { ICurrency, Currency } from '../currency.model';
import { CurrencyService } from '../service/currency.service';

@Component({
  selector: 'jhi-currency-update',
  templateUrl: './currency-update.component.html',
})
export class CurrencyUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    Code: [null, [Validators.required]],
    Name: [null, [Validators.required]],
    NameRu: [null, [Validators.required]],
    NameUz: [null, [Validators.required]],
    NameUzc: [null, [Validators.required]],
    NameEn: [null, [Validators.required]],
    Nominal: [null, [Validators.required]],
    Rate: [null, [Validators.required]],
    Diff: [null, [Validators.required]],
    Date: [null, [Validators.required]],
  });

  constructor(protected currencyService: CurrencyService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ currency }) => {
      this.updateForm(currency);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const currency = this.createFromForm();
    if (currency.id !== undefined) {
      this.subscribeToSaveResponse(this.currencyService.update(currency));
    } else {
      this.subscribeToSaveResponse(this.currencyService.create(currency));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICurrency>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(currency: ICurrency): void {
    this.editForm.patchValue({
      id: currency.id,
      Code: currency.Code,
      Name: currency.Ccy,
      NameRu: currency.CcyNm_RU,
      NameUz: currency.CcyNm_UZ,
      NameUzc: currency.CcyNm_UZC,
      NameEn: currency.CcyNm_EN,
      Nominal: currency.Nominal,
      Rate: currency.Rate,
      Diff: currency.Diff,
      Date: currency.Date,
    });
  }

  protected createFromForm(): {
    NameUz: any;
    Diff: any;
    Rate: any;
    NameEn: any;
    NameUzc: any;
    id: any;
    Code: any;
    NameRu: any;
    Nominal: any;
    Date: any;
    Name: any;
  } {
    return {
      ...new Currency(),
      id: this.editForm.get(['id'])!.value,
      Code: this.editForm.get(['Code'])!.value,
      Name: this.editForm.get(['Name'])!.value,
      NameRu: this.editForm.get(['NameRu'])!.value,
      NameUz: this.editForm.get(['NameUz'])!.value,
      NameUzc: this.editForm.get(['NameUzc'])!.value,
      NameEn: this.editForm.get(['NameEn'])!.value,
      Nominal: this.editForm.get(['Nominal'])!.value,
      Rate: this.editForm.get(['Rate'])!.value,
      Diff: this.editForm.get(['Diff'])!.value,
      Date: this.editForm.get(['Date'])!.value,
    };
  }
}
