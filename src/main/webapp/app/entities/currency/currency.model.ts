export interface ICurrency {
  id?: number;
  Code?: number;
  Ccy?: string;
  CcyNm_RU?: string;
  CcyNm_UZ?: string;
  CcyNm_UZC?: string;
  CcyNm_EN?: string;
  Nominal?: number;
  Rate?: number;
  Diff?: number;
  Date?: string;
}

export class Currency implements ICurrency {
  constructor(
    public id?: number,
    public Code?: number,
    public Name?: string,
    public NameRu?: string,
    public NameUz?: string,
    public NameUzc?: string,
    public NameEn?: string,
    public Nominal?: number,
    public Rate?: number,
    public Diff?: number,
    public Date?: string
  ) {}
}

export function getCurrencyIdentifier(currency: ICurrency): number | undefined {
  return currency.id;
}
