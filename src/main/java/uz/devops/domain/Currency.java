package uz.devops.domain;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A Currency.
 */
@Entity
@Table(name = "currency")
public class Currency {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "code", nullable = false)
    private Integer code;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "name_ru", nullable = false)
    private String nameRu;

    @NotNull
    @Column(name = "name_uz", nullable = false)
    private String nameUz;

    @NotNull
    @Column(name = "name_uzc", nullable = false)
    private String nameUzc;

    @NotNull
    @Column(name = "name_en", nullable = false)
    private String nameEn;

    @NotNull
    @Column(name = "nominal", nullable = false)
    private Integer nominal;

    @NotNull
    @Column(name = "rate", nullable = false)
    private Double rate;

    @NotNull
    @Column(name = "diff", nullable = false)
    private Double diff;

    @Column(name = "date", nullable = false)
    private LocalDate date;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Currency id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCode() {
        return this.code;
    }

    public Currency code(Integer code) {
        this.setCode(code);
        return this;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return this.name;
    }

    public Currency name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameRu() {
        return this.nameRu;
    }

    public Currency nameRu(String nameRu) {
        this.setNameRu(nameRu);
        return this;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public String getNameUz() {
        return this.nameUz;
    }

    public Currency nameUz(String nameUz) {
        this.setNameUz(nameUz);
        return this;
    }

    public void setNameUz(String nameUz) {
        this.nameUz = nameUz;
    }

    public String getNameUzc() {
        return this.nameUzc;
    }

    public Currency nameUzc(String nameUzc) {
        this.setNameUzc(nameUzc);
        return this;
    }

    public void setNameUzc(String nameUzc) {
        this.nameUzc = nameUzc;
    }

    public String getNameEn() {
        return this.nameEn;
    }

    public Currency nameEn(String nameEn) {
        this.setNameEn(nameEn);
        return this;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public Integer getNominal() {
        return this.nominal;
    }

    public Currency nominal(Integer nominal) {
        this.setNominal(nominal);
        return this;
    }

    public void setNominal(Integer nominal) {
        this.nominal = nominal;
    }

    public Double getRate() {
        return this.rate;
    }

    public Currency rate(Double rate) {
        this.setRate(rate);
        return this;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Double getDiff() {
        return this.diff;
    }

    public Currency diff(Double diff) {
        this.setDiff(diff);
        return this;
    }

    public void setDiff(Double diff) {
        this.diff = diff;
    }

    public LocalDate getDate() {
        return this.date;
    }

    public Currency date(LocalDate date) {
        this.setDate(date);
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Currency)) {
            return false;
        }
        return id != null && id.equals(((Currency) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Currency{" +
            "id=" + getId() +
            ", code=" + getCode() +
            ", name='" + getName() + "'" +
            ", nameRu='" + getNameRu() + "'" +
            ", nameUz='" + getNameUz() + "'" +
            ", nameUzc='" + getNameUzc() + "'" +
            ", nameEn='" + getNameEn() + "'" +
            ", nominal=" + getNominal() +
            ", rate=" + getRate() +
            ", diff=" + getDiff() +
            ", date='" + getDate() + "'" +
            "}";
    }
}
