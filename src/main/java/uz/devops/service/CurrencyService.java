package uz.devops.service;

import java.util.List;
import java.util.Optional;
import uz.devops.service.dto.CurrencyDTO;

public interface CurrencyService {
    CurrencyDTO save(CurrencyDTO currencyDTO);

    List<CurrencyDTO> saveAll();

    CurrencyDTO update(CurrencyDTO currencyDTO);

    List<CurrencyDTO> findAll();

    Optional<CurrencyDTO> findOne(Long id);

    void delete(Long id);
}
