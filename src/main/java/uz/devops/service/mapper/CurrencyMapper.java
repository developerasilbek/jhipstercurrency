package uz.devops.service.mapper;

import java.util.List;
import org.mapstruct.*;
import uz.devops.domain.Currency;
import uz.devops.service.dto.CurrencyDTO;

@Mapper(componentModel = "spring")
public interface CurrencyMapper {
    @Mapping(target = "date", source = "date", dateFormat = "dd.MM.yyyy")
    Currency toEntity(CurrencyDTO currencyDTO);

    @Mapping(target = "date", source = "date", dateFormat = "dd.MM.yyyy")
    CurrencyDTO toDto(Currency currency);

    List<Currency> toEntity(List<CurrencyDTO> dtoList);

    List<CurrencyDTO> toDto(List<Currency> entityList);
}
