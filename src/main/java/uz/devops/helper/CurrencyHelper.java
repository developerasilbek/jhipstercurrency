package uz.devops.helper;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import uz.devops.service.dto.CurrencyDTO;

@Component
public class CurrencyHelper {

    @Value("${currency.cbu.url}")
    private String cbuURL;

    private final RestTemplate restTemplate;

    public CurrencyHelper(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public List<CurrencyDTO> getCurrenciesFromCbu() {
        HttpEntity<List<CurrencyDTO>> request = new HttpEntity<>(new ArrayList<>());
        ResponseEntity<List<CurrencyDTO>> response = restTemplate.exchange(
            cbuURL,
            HttpMethod.GET,
            request,
            new ParameterizedTypeReference<>() {}
        );
        return response.getBody();
    }
}
